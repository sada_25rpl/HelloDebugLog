package id.sch.smktelkom_mlg.learn.hellodebuglog;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import static id.sch.smktelkom_mlg.learn.hellodebuglog.R.layout.activity_main;

public class Main2Activity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);

        TextView tvHello = null;
        TextView tvDua;


        tvDua = findViewById(R.id.textViewDua);
        tvHello = findViewById(R.id.textViewHello);

        Log.d("COBADEBUG", "tvHello" + tvHello);
        Log.d("COBADEBUG", "tvDua" + tvDua);

        tvHello.setText("Ini Hello " + tvDua.getText());

    }
}
